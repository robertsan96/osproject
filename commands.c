char *getArgumentByKey(int argc, char** args, char *key) {
  for(int index = 0; index <= argc; index++) {
    if(strncmp(args[index], key, strlen(key)) == 0) {
      // The key was found, we get the argument. (if there is one)
      char *argument = malloc(sizeof(argc));
      if(args[index+1] != NULL) {
        strcpy(argument, args[index+1]);
        return argument;
      } else {
        return NULL;
      }
    }
  }
  return NULL;
}

int arrayHasKey(int argc, char** args, char *key) {
  for(int index = 0; index < argc; index++) {
    if(strcmp(args[index], key) == 0) {
      return 1;
    }
  }
  return 0;
}

// Version information
char author[] = "Sandru Robert-Dumitru";
char version[] = "1.12";
char date[] = "12 Jan 2016";
char versionInformation[] = "Thank you for your interest! This piece of code is going to be my project for Operating Systems course.";

void cmdExit(int withCode) {
  exit(withCode);
}

void cmdDir(char *path) {
  DIR *dir;
  struct dirent *ent;
  char processedPath[4096];
  strcpy(processedPath, path);
  if ((dir = opendir(processedPath)) != NULL) {
    while ((ent = readdir (dir)) != NULL) {
      printf ("%s\n", ent->d_name);
    }
    closedir (dir);
  } else {
    perror ("");
  }
}

void cmdExternal() {

}

void cmdHelp() {
  puts("Available commands: \n\
  version - displays information about this version. \n\
  rm - removes files or directories. (-i, -r (-R), -v) \n\
  mv - moves or renames files. (-i, -t, -s) \n\
  dir - displays a list of a directory's files and subdirectories.");
}

void cmdVersion() {
  printf("\tAuthor: %s \n\tVersion: %s \n\tDate: %s \n\n\t%s\n",
          author, version, date, versionInformation);
}

char *getCurrentWorkingDirectory() {
  char buff[4096] = "";
  char *currentDir = getcwd(buff, 4096);
  return currentDir;
}

int setCurrentWorkingDirectory(char *path) {
  if(chdir(path) == 0) return 1;
  else return 0;
}

void removeSpaces(char* source)
{
  char* i = source;
  char* j = source;
  while(*j != 0)
  {
    *i = *j++;
    if(*i != ' ')
      i++;
  }
  *i = 0;
}

void cmdCd(int argc, char** args) {
  char *toPath = args[1];
  if(toPath != NULL) {
    char *currentWorkingDirectory = getCurrentWorkingDirectory();
    strcat(currentWorkingDirectory, "/");
    strcat(currentWorkingDirectory, toPath);
    setCurrentWorkingDirectory(currentWorkingDirectory);
  }
}

int cmdMv(int argc, char ** args) {
  if(argc < 3) { return 0; }
  if(argc == 3) {
    // mv file destination
    char command[1024] = "mv ";
    strcat(command, args[1]);
    strcat(command, " ");
    strcat(command, args[2]);
    system(command);
  }
}

int cmdRm(int argc, char** args) {
  if(argc < 2) return 0;
  int verboseMode = arrayHasKey(argc, args, "-v") ? 1 : 0;
  int interactiveMode = arrayHasKey(argc, args, "-i") ? 1 : 0;
  int recursiveMode = arrayHasKey(argc, args, "-r") ? 1 : 0;
  if (!recursiveMode) {
    recursiveMode = arrayHasKey(argc, args, "-R") ? 1 : 0;
  }

  // rm file -i -r -v
  if(!interactiveMode) {
    // NON interactive mode
    if(access(args[1], F_OK) != -1) {
      if(remove(args[1]) == 0) {
        if(verboseMode) {
          char message[1024] = "Removed ";
          strcat(message, args[1]);
          printf("%s\n",message);
        }
      }
      else
        if(verboseMode) {
          char message[1024] = "Could not remove ";
          strcat(message, args[1]);
          printf("%s\n",message);
        }
    } else {
      puts("File does not exist.");
    }
  } else {
    // interactive mode
    if(access(args[1], F_OK) != -1) {
      // file exists
      int confirmValid = 0;
      do {
        char message[1024] = "Are you sure you want to delete '";
        strcat(message, args[1]);
        strcat(message, "'? [y/n]");
        printf("%s\n",message);
        char *confirm = readline("$> ");
        if(strcmp(confirm, "y") == 0) {
          confirmValid = 1;
          if(remove(args[1]) == 0) {
            if(verboseMode) {
              char message[1024] = "Removed ";
              strcat(message, args[1]);
              printf("%s\n",message);
            }
          } else {
            if(verboseMode) {
              char message[1024] = "Could not remove ";
              strcat(message, args[1]);
              printf("%s\n",message);
            }
          }
        } else if(strcmp(confirm, "n") == 0) {
          confirmValid = 1;
        } else {
          confirmValid = 0;
        }
      } while(confirmValid == 0);
    } else {
      puts("The file does not exist.");
    }
  }

}
