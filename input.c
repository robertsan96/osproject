void processInput(char *input) {
  char *processedInput = malloc(sizeof(input) * 4096);
  memcpy(processedInput, input, sizeof(processedInput) * 4096);
  if ((strlen(processedInput) > 0) &&
      (processedInput[strlen(processedInput) - 1] == '\n')) {
      processedInput[strlen(processedInput) - 1] = '\0';
  }
  int argc = numberOfArguments(processedInput);

  char **args = buildArgumentsArray(processedInput);
  if(argc > 0) {
    if(strcmp(args[0], "exit") == 0) cmdExit(1);
    else if(strcmp(args[0], "rm") == 0) cmdRm(argc, args);
    else if(strcmp(args[0], "mv") == 0) cmdMv(argc, args);
    else if(strcmp(args[0], "cd") == 0) cmdCd(argc, args);
    else if(strcmp(args[0], "dir") == 0) cmdDir(getCurrentWorkingDirectory());
    else if(strcmp(args[0], "help") == 0) cmdHelp();
    else if(strcmp(args[0], "version") == 0) cmdVersion();
    else {
      // Not recognized
      system(input);

    }
  }
  free(args);
}

void receiveInput() {
  do {
    char *input = readline("$> ");
    add_history(input);
    processInput(input);
    free(input);
  } while(1);
}
