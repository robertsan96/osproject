int numberOfArguments(char *input) {
  int arguments = 0;
  for(int index = 0; index <= strlen(input); index++) {
    if(input[index] == ' ' || index+1 == strlen(input)) {
      arguments++;
    }
  }
  return arguments;
}

char **buildArgumentsArray(char *input) {
  int argc = numberOfArguments(input);
  char **args;
  args = malloc(1024 * sizeof(char*));

  int argumentsProcessed = 0;
  int index = 0;
  if(strlen(input) > 0) {
    while(strlen(input) > 0 && (index < strlen(input))) {
      if(input[index] == ' ' || (index == strlen(input)-1)) {
        if(input[index+1] == ' ') { puts("Invalid command"); return NULL; }
        char argument[1024] = "";
        strncpy(argument, input, index+1);
        argument[strlen(argument)+1] = '\0';
        strncpy(input, &input[index+1], strlen(input));
        input[strlen(input)+1] = '\0';
        removeSpaces(argument);
        args[argumentsProcessed] = malloc(sizeof(argument) * 1024);
        memcpy(args[argumentsProcessed], argument, sizeof(args[argumentsProcessed]));
        argumentsProcessed++;
        index = 0;
      }
      index++;
    }
    return args;
  } else {
    return NULL;
  }
}
