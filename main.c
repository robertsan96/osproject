#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <termios.h>
#include <unistd.h>
#include <readline/readline.h>
#include <readline/history.h>
#include <dirent.h>
#include <unistd.h>

#include "commands.c"
#include "arguments.c"
#include "input.c" // receiveInput()

int main() {
  puts("Hello. Use \"help\" for more information.");
  receiveInput();
}
